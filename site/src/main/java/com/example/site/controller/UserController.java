package com.example.site.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.example.site.model.User;

@Controller
public class UserController {
	
//	@Autowired
//	User user;
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home() {
		return "index.html";
	}
	
	@RequestMapping(value = "/createUser")
	public String createUser() {
		return "userCreated";
	}
	
	@RequestMapping(value = "/getUser/{userId}")
	public String getUser(@PathVariable("userId") int userId) {
		return "user" + userId;
	}

}
