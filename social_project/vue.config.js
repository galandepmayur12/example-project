module.exports = {
    devServer: {
        port: 8081,
        proxy: {
            '/api': {
                target: 'http://localhost:8880',
                ws: true,
                changeOrigin: true
            }
        }
    }
}